package com.gmail.ivanov.erik.coverageArea.mapper;

import com.gmail.ivanov.erik.coverageArea.loadConfig.ConfigBuilder;
import liquibase.exception.LiquibaseException;
import org.apache.ibatis.builder.xml.XMLMapperBuilder;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.sql.SQLException;
import java.util.List;


public class BaseMapper<T> {

    public static SqlSessionFactory sf = null;
    protected static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    protected SqlSession session;

    private static Configuration getConf() {
        if (sf == null) {
            log.info("starting up myBatis SessionFactory");
            Configuration configuration = null;
            try {
                configuration = ConfigBuilder.getConfiguration();
            } catch (SQLException | LiquibaseException e) {
                throw new RuntimeException(e.getMessage());
            }
            sf = new SqlSessionFactoryBuilder().build(configuration);
        }
        return sf.getConfiguration();
    }

    private static InputStream getPathToMapper(String mapperXml) {
        String xmlName = "/" + mapperXml.replace(".", "/") + ".xml";
        InputStream input = MethodHandles.lookup().lookupClass().getClass().getResourceAsStream(xmlName);
        if (input == null) {
            input = MethodHandles.publicLookup().lookupClass().getResourceAsStream(xmlName);
        }
        if (input == null) {
            throw new RuntimeException("ERROR: not found mapper, " + xmlName);
        }
        return input;
    }

    public static void loadMapper(Configuration conf, String mapperClass) {
        // trick to use database-specific XML files for a single Mapper Java interface
         try (InputStream input = getPathToMapper(mapperClass)) {
            XMLMapperBuilder mapperParser = new XMLMapperBuilder(input, conf, mapperClass, conf.getSqlFragments());
            mapperParser.parse();
            conf.addLoadedResource(mapperClass);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static void loadMapper(Configuration conf, Class mapperClass) {
        loadMapper(conf, mapperClass.getName());
    }

    public static void loadMappers(List<String> mappersClass) throws IOException {
        mappersClass.forEach(item -> loadMapper(getConf(), item));
    }

    @After
    public void after() {
        try {
            session.close();
            log.debug("close Session");
        } catch (Exception e) {
            log.debug("cannot close Session");
        }
    }

    public <T> T getAction(Class<T> type) {
        if (session == null) {
            session = sf.openSession();
            log.debug("openSession");
        }
        return session.getMapper(type);
    }
}
