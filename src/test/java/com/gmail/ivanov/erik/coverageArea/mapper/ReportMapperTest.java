package com.gmail.ivanov.erik.coverageArea.mapper;

import com.gmail.ivanov.erik.coverageArea.domain.param.MapParam;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.BaseStation;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.CalculateReport;
import com.gmail.ivanov.erik.coverageArea.helper.GenerateData;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class ReportMapperTest extends BaseMapper {


    @Before
    public void setUp() throws Exception {
        loadMappers( Arrays.asList(Common.class.getName(), ReportMapper.class.getName(), BaseStationMapper.class.getName(), MobileStationMapper.class.getName()));
        session = sf.openSession();
    }

    @Test
    public void count() {
        final ReportMapper mapper = session.getMapper(ReportMapper.class);
        final MapParam param = new MapParam();
        final Integer result = mapper.count(param);
        assertNotNull(result);
    }

    private GenerateData insertData(BaseStationMapper baseStationMapper, MobileStationMapper mobileStationMapper, ReportMapper reportMapper,
                                    int limitReport, int limitMessage) {
        GenerateData generateData = new GenerateData();
        generateData.generate(10, 3, limitReport, limitMessage);

        generateData.getBaseStationList().forEach(baseStationMapper::insert);
        generateData.getMobileStationList().forEach(mobileStationMapper::insert);
        generateData.getReportList().forEach(reportMapper::insert);
        return generateData;
    }
    @Test
    public void insert() {
        final BaseStationMapper baseStationMapper = session.getMapper(BaseStationMapper.class);
        final MobileStationMapper mobileStationMapper = session.getMapper(MobileStationMapper.class);
        final ReportMapper reportMapper = session.getMapper(ReportMapper.class);
        int limitReport = 10;
        int limitMessage = 10;

        GenerateData generateData = insertData(baseStationMapper, mobileStationMapper, reportMapper, limitReport, limitMessage);

        final MapParam param = new MapParam();
        List<Object> listId = generateData.getBaseStationList().stream()
                .map(BaseStation::getId)
                .collect(Collectors.toList());
        param.addCriteriaIn("report.base_station_id", listId);
        param.prepareValue();
        final Integer countReport = reportMapper.count(param);
        assertEquals(java.util.Optional.of(limitReport*limitMessage), java.util.Optional.ofNullable(countReport));
    }

    @Test
    public void getMobileStation() {
        final BaseStationMapper baseStationMapper = session.getMapper(BaseStationMapper.class);
        final MobileStationMapper mobileStationMapper = session.getMapper(MobileStationMapper.class);
        final ReportMapper reportMapper = session.getMapper(ReportMapper.class);
        GenerateData generateData = insertData(baseStationMapper, mobileStationMapper, reportMapper, 10, 10);

        MapParam param = new MapParam();
        param.setFilter("station", generateData.getReportList().get(0).getReports().get(0).getMobile_station_id());
        List<CalculateReport> list = reportMapper.getMessage(param);
        assertNotNull(list);

        list.forEach(e-> {
            log.debug("base_station:{}, dist:{}, x:{}, y{}", e.getBase_station_id(), e.getDetectionRadiusInMeters(), e.getX(), e.getY());
            e.getReports().forEach(item -> {
                log.debug("Mobile_station:{}, dist:{}, Timestamp:{}", item.getMobile_station_id(), item.getDistance(), item.getTimestamp() );
            });
        });


    }
}

//        generateData.getBaseStationList().forEach(e -> {
//            param.addCriteria(new Filter("report.base_station_id", e.getId().toString(),  "=", Filter.SqlSuffix.OR));
//        });

