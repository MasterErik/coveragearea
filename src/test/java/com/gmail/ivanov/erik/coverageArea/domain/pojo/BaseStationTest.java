package com.gmail.ivanov.erik.coverageArea.domain.pojo;

import com.gmail.ivanov.erik.coverageArea.util.TestUtil;
import org.junit.Test;

import java.util.UUID;

import static com.gmail.ivanov.erik.coverageArea.domain.Constant.EPSILON;
import static org.junit.Assert.assertEquals;

public class BaseStationTest {

    @Test
    public void checkPojo() throws Exception {
        TestUtil.equalsVerifier(BaseStation.class);
    }

    @Test
    public void calculateDistance() throws Exception {
        MobileStation mobileStation = new MobileStation(UUID.randomUUID(), 10, 10);
        BaseStation baseStation = new BaseStation("Base", 10, UUID.randomUUID(), 5, 5);
        Float distance = baseStation.calculateDistance(mobileStation);
        assertEquals(7.0710678d, Double.valueOf(distance),  EPSILON);
    }
}
