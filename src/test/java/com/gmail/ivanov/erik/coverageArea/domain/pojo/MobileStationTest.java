package com.gmail.ivanov.erik.coverageArea.domain.pojo;

import com.gmail.ivanov.erik.coverageArea.util.TestUtil;
import org.junit.Test;

public class MobileStationTest {
    @Test
    public void checkPojo() throws Exception {
        TestUtil.equalsVerifier(MobileStation.class);
    }

}
