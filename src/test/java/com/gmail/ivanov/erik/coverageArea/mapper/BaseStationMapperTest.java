package com.gmail.ivanov.erik.coverageArea.mapper;

import com.gmail.ivanov.erik.coverageArea.domain.param.Filter;
import com.gmail.ivanov.erik.coverageArea.domain.param.MapParam;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.BaseStation;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class BaseStationMapperTest extends BaseMapper {


    @Before
    public void setUp() throws Exception {
        loadMappers( Arrays.asList(Common.class.getName(), BaseStationMapper.class.getName()));
        session = sf.openSession();
    }

    @Test
    public void count() {
        final BaseStationMapper mapper = session.getMapper(BaseStationMapper.class);
        final MapParam param = new MapParam();
        final Integer result = mapper.count(param);
        assertNotNull(result);
    }

    @Test
    public void insert() {
        final BaseStationMapper baseStationMapper = session.getMapper(BaseStationMapper.class);
        final UUID baseStationId = UUID.randomUUID();
        final BaseStation baseStation = new BaseStation("Base1", 10, baseStationId, 1, 1);
        baseStationMapper.insert(baseStation);

        final MapParam param = new MapParam();
        param.addCriteria(new Filter("baseS.id", baseStationId.toString(),  "=", Filter.SqlSuffix.CLOSE));
        final Integer count = baseStationMapper.count(param);
        assertEquals(java.util.Optional.of(1), java.util.Optional.ofNullable(count));
    }

}
