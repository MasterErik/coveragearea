package com.gmail.ivanov.erik.coverageArea.helper;

import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class GenerateDataTest {
    private static GenerateData generateData;

    @Test
    public void genBaseStation() throws Exception {
        generateData = new GenerateData();
        generateData.genBaseStation(2);
        assertEquals(java.util.Optional.of(2), java.util.Optional.of(generateData.getBaseStationList().size()));
    }

    @Test
    public void genMobileStation() throws Exception {
        generateData = new GenerateData();
        generateData.genMobileStation(2);
        assertEquals(java.util.Optional.of(2), java.util.Optional.of(generateData.getMobileStationList().size()));
    }

    @Test
    public void getRandomBaseStation() throws Exception {
        generateData = new GenerateData();
        generateData.genBaseStation(1);
        UUID id = generateData.getRandomBaseStation();
        assertNotNull(id);
    }

    @Test
    public void getRandomMobileStation() throws Exception {
        generateData = new GenerateData();
        generateData.genMobileStation(1);
        UUID id = generateData.getRandomMobileStation();
        assertNotNull(id);
    }

    @Test
    public void genReport() throws Exception {
        generateData = new GenerateData();
        generateData.generate(1, 1, 1, 1);
        assertNotNull(generateData.getReportList());
        assertNotNull(generateData.getBaseStationList());
        assertNotNull(generateData.getMobileStationList());

        UUID mobileStation = generateData.getMobileStationList().get(0).getId();
        UUID baseStation = generateData.getBaseStationList().get(0).getId();
        assertEquals(java.util.Optional.of(baseStation), java.util.Optional.of(generateData.getReportList().get(0).getBase_station_id()));
        assertEquals(java.util.Optional.of(mobileStation), java.util.Optional.of(generateData.getReportList().get(0).getReports().get(0).getMobile_station_id()));

    }

    @Test
    public void generate() throws Exception {
        generateData = new GenerateData();
        generateData.generate(5, 8, 20, 10);
        assertEquals(java.util.Optional.of(5), java.util.Optional.of(generateData.getBaseStationList().size()));
        assertEquals(java.util.Optional.of(8), java.util.Optional.of(generateData.getMobileStationList().size()));
        assertEquals(java.util.Optional.of(20), java.util.Optional.of(generateData.getReportList().size()));
        assertEquals(java.util.Optional.of(10), java.util.Optional.of(generateData.getReportList().get(0).getReports().size()));
    }

}