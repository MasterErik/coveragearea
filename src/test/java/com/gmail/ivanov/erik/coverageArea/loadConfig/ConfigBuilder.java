package com.gmail.ivanov.erik.coverageArea.loadConfig;

import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.FileSystemResourceAccessor;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.apache.tomcat.dbcp.dbcp.BasicDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

import static com.gmail.ivanov.erik.coverageArea.config.TypeHandlerConfiguration.initConfig;


public class ConfigBuilder {

    public static Configuration getConfiguration() throws SQLException, LiquibaseException {
        DataSource dataSource = getDataSource();
        updateLiquedBase(dataSource.getConnection());

        TransactionFactory trxFactory = new JdbcTransactionFactory();

        // Creates an environment object with the specified name, transaction
        // factory and a data source.
        Environment env = new Environment("mybatis", trxFactory, dataSource);

        // Creates a Configuration object base on the Environment object.
        Configuration config = new Configuration(env);
        initConfig(config);
        return config;
    }


    private static DataSource getDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(Constant.DATASOURCE_DRIVER);
        dataSource.setUrl(Constant.DATASOURCE_URL);
        dataSource.setUsername(Constant.DATASOURCE_USER);
        dataSource.setPassword(Constant.DATASOURCE_PASSWORD);
        dataSource.setDefaultAutoCommit(false);

        return dataSource;
    }

    private static void updateLiquedBase(Connection connection) throws LiquibaseException {
        Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
        Liquibase liquibase = new Liquibase(Constant.CHANGELOG, new FileSystemResourceAccessor(), database);
        //liquibase.dropAll();
        //liquibase.clearCheckSums();
        liquibase.update(new Contexts(), new LabelExpression());
    }

}
