package com.gmail.ivanov.erik.coverageArea.service.impl;

import com.gmail.ivanov.erik.coverageArea.domain.Errors;
import com.gmail.ivanov.erik.coverageArea.domain.param.MapParam;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.CalculateReport;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.Message;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.MobileStation;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.ReportMessage;
import com.gmail.ivanov.erik.coverageArea.mapper.MobileStationMapper;
import com.gmail.ivanov.erik.coverageArea.mapper.ReportMapper;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class MobileServiceImplTest {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private static MobileServiceImpl mobileServiceImpl;
    private static ReportMapper reportMapper;
    private static MobileStationMapper mobileStationMapper;
    private static final UUID mobileStationId = UUID.fromString("d7ce31bb-8e1d-448c-b430-2f4e7aa6d8b6");
    private static final UUID baseStationId = UUID.fromString("cdfd647b-95c8-4e8f-98a3-ffd9cd095d14");

    @BeforeClass
    public static void setUpClass() throws Exception {
        reportMapper = mock(ReportMapper.class);
        mobileStationMapper = mock(MobileStationMapper.class);
        mobileServiceImpl = new MobileServiceImpl(reportMapper, mobileStationMapper);
    }
    @Test
    public void insert() throws Exception {
        ReportMessage reportMessage = new ReportMessage(baseStationId);
        mobileServiceImpl.insert(reportMessage);
        verify(reportMapper, times(1)).insert(reportMessage);
    }

    private MobileStation executeCoordinateMethod(List<CalculateReport> list, MobileStation mobileStation) {
        when(mobileStationMapper.getOne(any(MapParam.class))).thenReturn(mobileStation);
        when(reportMapper.getMessage(any(MapParam.class))).thenReturn(list);
        return mobileServiceImpl.getCoordinatesMobileStation(mobileStationId);
    }

    @Test
    public void getCoordinatesMobileStationErrorDuplicateMessage() throws Exception {
        MobileStation mobileStation = new MobileStation(mobileStationId, 10f, 10f);
        List<CalculateReport> list = new ArrayList<>();
        CalculateReport calculateReport = new CalculateReport(baseStationId, 200f, 0f, 0f);
        calculateReport.getReports().add(new Message(mobileStationId, 2f, new Timestamp(0)));
        calculateReport.getReports().add(new Message(mobileStationId, 2f, new Timestamp(0)));
        list.add(calculateReport);

        MobileStation result = executeCoordinateMethod(list, mobileStation);
        assertEquals(java.util.Optional.of(Errors.REPEATEDLY.getId()), java.util.Optional.ofNullable(result.getError_code()));
    }
    @Test
    public void getCoordinatesMobileStationErrorNotFound() throws Exception {
        MobileStation mobileStation = new MobileStation(mobileStationId, 10f, 10f);
        List<CalculateReport> list = new ArrayList<>();
        CalculateReport calculateReport = new CalculateReport(baseStationId, 200f, 0f, 0f);
        list.add(calculateReport);

        MobileStation result = executeCoordinateMethod(list, mobileStation);
        assertEquals(java.util.Optional.of(Errors.NOT_FOUND.getId()), java.util.Optional.ofNullable(result.getError_code()));
    }
    @Test
    public void getCoordinatesMobileStationErrorRange() throws Exception {
        MobileStation mobileStation = new MobileStation(mobileStationId, 10f, 10f);
        List<CalculateReport> list = new ArrayList<>();
        CalculateReport calculateReport = new CalculateReport(baseStationId, 1f, 0f, 0f);
        calculateReport.getReports().add(new Message(mobileStationId, 2f, new Timestamp(0)));
        list.add(calculateReport);

        MobileStation result = executeCoordinateMethod(list, mobileStation);
        assertEquals(java.util.Optional.of(Errors.DETECTION_RANGE.getId()), java.util.Optional.ofNullable(result.getError_code()));
    }
    @Test
    public void getCoordinatesMobileStationErrorCalcDistance() throws Exception {
        MobileStation mobileStation = new MobileStation(mobileStationId, 10f, 10f);
        List<CalculateReport> list = new ArrayList<>();
        CalculateReport calculateReport = new CalculateReport(baseStationId, 100f, 0f, 0f);
        calculateReport.getReports().add(new Message(mobileStationId, 2f, new Timestamp(0)));
        list.add(calculateReport);

        MobileStation result = executeCoordinateMethod(list, mobileStation);
        assertEquals(java.util.Optional.of(Errors.CALC_DISTANCE.getId()), java.util.Optional.ofNullable(result.getError_code()));
    }

    @Test
    public void getCoordinatesMobileStation() throws Exception {
        MobileStation mobileStation = new MobileStation(mobileStationId, 10f, 10f);
        List<CalculateReport> list = new ArrayList<>();
        CalculateReport calculateReport = new CalculateReport(baseStationId, 100f, 0f, 0f);
        calculateReport.getReports().add(new Message(mobileStationId, 14.142136f, new Timestamp(0)));
        list.add(calculateReport);

        MobileStation result = executeCoordinateMethod(list, mobileStation);
        assertNull(result.getError_code());
    }
}