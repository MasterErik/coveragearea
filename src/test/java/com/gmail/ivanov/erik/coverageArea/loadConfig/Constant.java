package com.gmail.ivanov.erik.coverageArea.loadConfig;

public class Constant {
    public static final String DATASOURCE_URL = "jdbc:h2:mem:coverageArea;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE";
    public static final String DATASOURCE_DRIVER = "org.h2.Driver";
    public static final String DATASOURCE_USER = "admin";
    public static final String DATASOURCE_PASSWORD = "admin";

    public static final String CHANGELOG = "src/main/resources/db/master.xml";
}
