package com.gmail.ivanov.erik.coverageArea.controller;

import com.gmail.ivanov.erik.coverageArea.CoverageAreaApplication;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.Message;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.MobileStation;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.ReportMessage;
import com.gmail.ivanov.erik.coverageArea.service.impl.MobileServiceImpl;
import com.gmail.ivanov.erik.coverageArea.util.TestUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.sql.Timestamp;
import java.util.UUID;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CoverageAreaApplication.class)
public class StationControllerTest {

    private MockMvc restMockMvc;
    private MobileServiceImpl mobileService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mobileService = mock(MobileServiceImpl.class);

        StationController stationController = new StationController(mobileService);
        restMockMvc = MockMvcBuilders
                .standaloneSetup(stationController)
                .build();
    }

    @Test
    public void restEndpoint1() throws Exception {
        ReportMessage reportMessage = new ReportMessage(UUID.fromString("47b4b23e-71d4-4a24-bb56-e6d9f37273c4"));
        reportMessage.getReports().add(new Message(UUID.fromString("47b4b23e-71d4-4a24-bb56-e6d9f37273c1"), 1f, new Timestamp(100)));
        reportMessage.getReports().add(new Message(UUID.fromString("47b4b23e-71d4-4a24-bb56-e6d9f37273c2"), 2f, new Timestamp(100)));
        restMockMvc.perform(
                post("/RestEndpoint1")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(reportMessage))
                )
            .andExpect(status().isOk());
    }

    @Test
    public void restEndpoint2() throws Exception {
        UUID mobilId = UUID.fromString("47b4b23e-71d4-4a24-bb56-e6d9f37273c4");
        MobileStation mobileStation = new MobileStation(mobilId, 10f, 10f);
        when(mobileService.getCoordinatesMobileStation(mobilId)).thenReturn(mobileStation);
        restMockMvc.perform(
                get("/RestEndpoint2/{mobileId}", mobilId.toString()))
                .andExpect(content()
                        .json(
                        "{'error_radius':null,'error_code':null,'error_description':null" +
                            ",'id':'47b4b23e-71d4-4a24-bb56-e6d9f37273c4','x':10.0,'y':10.0,'mobileId':'47b4b23e-71d4-4a24-bb56-e6d9f37273c4'}"
                        )
                )
                .andExpect(status().isOk());
    }

}