package com.gmail.ivanov.erik.coverageArea.mapper;

import com.gmail.ivanov.erik.coverageArea.domain.param.Filter;
import com.gmail.ivanov.erik.coverageArea.domain.param.MapParam;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.MobileStation;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class MobileStationMapperTest extends BaseMapper {


    @Before
    public void setUp() throws Exception {
        loadMappers( Arrays.asList(Common.class.getName(), MobileStationMapper.class.getName()));
        session = sf.openSession();
    }

    @Test
    public void count() {
        final MobileStationMapper mapper = session.getMapper(MobileStationMapper.class);
        final MapParam param = new MapParam();
        final Integer result = mapper.count(param);
        assertNotNull(result);
    }

    @Test
    public void insert() {
        final MobileStationMapper mobileStationMapper = session.getMapper(MobileStationMapper.class);
        final UUID id = UUID.randomUUID();
        final MobileStation mobileStation = new MobileStation(id, 1, 1);
        mobileStationMapper.insert(mobileStation);

        final MapParam param = new MapParam();
        param.addCriteria(new Filter("mobS.id", id.toString(),  "=", Filter.SqlSuffix.CLOSE));
        final Integer count = mobileStationMapper.count(param);
        assertEquals(java.util.Optional.of(1), java.util.Optional.ofNullable(count));
    }

}
