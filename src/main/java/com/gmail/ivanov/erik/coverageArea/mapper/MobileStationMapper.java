package com.gmail.ivanov.erik.coverageArea.mapper;

import com.gmail.ivanov.erik.coverageArea.domain.param.MapParam;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.MobileStation;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface MobileStationMapper {
    Integer count(MapParam param);
    void insert(MobileStation mobileStation);
    MobileStation getOne(MapParam param);
}
