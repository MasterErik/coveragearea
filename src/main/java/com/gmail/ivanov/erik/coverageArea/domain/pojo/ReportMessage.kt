package com.gmail.ivanov.erik.coverageArea.domain.pojo

import java.util.*

data class ReportMessage(var base_station_id: UUID, var reports: List<Message>) {
    constructor() : this(UUID(0,0), ArrayList())
    constructor(base_station_id: UUID) : this(base_station_id, ArrayList())
}
