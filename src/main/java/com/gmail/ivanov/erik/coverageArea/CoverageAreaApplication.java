package com.gmail.ivanov.erik.coverageArea;

import com.gmail.ivanov.erik.coverageArea.service.InitService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class CoverageAreaApplication {

    private static final Logger log = LoggerFactory.getLogger(CoverageAreaApplication.class);

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(CoverageAreaApplication.class);
        ConfigurableApplicationContext appContext = app.run(args);
        Environment env = appContext.getEnvironment();

        InitService initService = appContext.getBean(InitService.class);
        initService.initialize();
        log.info("\n----------------------------------------------------------\n\t"
                        + "Application '{}' is running! H2 Console URL:\n\t" + "Local: \t\t"
                        + "http://127.0.0.1:{}{}\n"
                        + "----------------------------------------------------------\n",
                env.getProperty("spring.application.name"),
                env.getProperty("server.port"),
                env.getProperty("spring.h2.console.path"));

	}
}
