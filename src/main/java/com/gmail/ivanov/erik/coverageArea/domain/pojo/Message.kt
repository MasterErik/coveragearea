package com.gmail.ivanov.erik.coverageArea.domain.pojo

import java.sql.Timestamp
import java.util.*

data class Message(var mobile_station_id: UUID, var distance: Float, var timestamp: Timestamp) {
    constructor(): this(UUID(0L, 0L), 0f, Timestamp(0))
}
