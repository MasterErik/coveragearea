package com.gmail.ivanov.erik.coverageArea.domain.pojo

import java.util.*

data class CalculateReport(var base_station_id: UUID, var detectionRadiusInMeters: Float, var x: Float, var y: Float, var reports: List<Message>) {
    constructor() : this(UUID(0,0), 0f, 0f, 0f, ArrayList())
    constructor(base_station_id: UUID, detectionRadiusInMeters: Float, x: Float, y: Float) : this(base_station_id, detectionRadiusInMeters, x, y, ArrayList())
}
