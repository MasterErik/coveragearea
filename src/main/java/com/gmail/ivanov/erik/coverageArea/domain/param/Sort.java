package com.gmail.ivanov.erik.coverageArea.domain.param;

import java.util.ArrayList;
import java.util.List;

public class Sort {
    private List<Sorting> orderBy;

    public static class Sorting {
        private String sortBy;
        private String sortOrder;

        public Sorting(String sortBy, String sortOrder) {
            this.setSortBy(sortBy);
            this.setSortOrder(sortOrder);
        }

        public String getSortBy() {
            return sortBy;
        }

        public void setSortBy(String sortBy) {
            this.sortBy = sortBy;
        }

        public String getSortOrder() {
            return sortOrder;
        }

        public void setSortOrder(String sortOrder) {
            //guard against sql-injection (sort order goes as string)
            if (!sortOrder.equalsIgnoreCase("ASC") && !sortOrder.equalsIgnoreCase("DESC")) {
                throw new RuntimeException("tried to set unknown sort order: " + sortOrder + ". only ASC and DESC allowed.");
            }
            this.sortOrder = sortOrder;
        }

        @Override
        public String toString() {
            return sortBy + " " + sortOrder;
        }
    }

    public void addOrderBy(String sortBy, String sortOrder) {
        if (orderBy == null) {
            orderBy = new ArrayList<>();
        }
        orderBy.add(new Sorting(sortBy, sortOrder));
    }

    public void setOrderBy(List<Sorting> orderBy) {
        this.orderBy = orderBy;
    }
}

