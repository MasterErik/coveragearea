package com.gmail.ivanov.erik.coverageArea.helper;

import java.util.concurrent.ThreadLocalRandom;

public class Random {
    public static int rnd(int max) {
        return ThreadLocalRandom.current().nextInt(max);
    }
}
