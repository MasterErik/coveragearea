package com.gmail.ivanov.erik.coverageArea.domain;

public class Constant {
    public static final String DEFAULT_CRITERIA = "filters";
    public static final String TABLE_NAME = "tableName";
    public static final Double EPSILON = 5.96e-08;

}
