package com.gmail.ivanov.erik.coverageArea.controller;


import com.gmail.ivanov.erik.coverageArea.domain.pojo.MobileStation;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.ReportMessage;
import com.gmail.ivanov.erik.coverageArea.service.MobileService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
public class StationController {
    private final MobileService mobileService;

    public StationController(MobileService mobileService) {
        this.mobileService = mobileService;
    }


    @PostMapping("/RestEndpoint1")
    @ResponseStatus(HttpStatus.OK)
    public void RestEndpoint1(@RequestBody ReportMessage reportMassage) {
        mobileService.insert(reportMassage);
    }

    @GetMapping("/RestEndpoint2/{mobileId}")
    public MobileStation RestEndpoint2(@PathVariable UUID mobileId) {
        return mobileService.getCoordinatesMobileStation(mobileId);
    }

}
