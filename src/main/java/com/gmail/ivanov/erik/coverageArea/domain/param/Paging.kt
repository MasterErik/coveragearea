package com.gmail.ivanov.erik.coverageArea.domain.param

open class Paging : Sort() {

    var limit: Int? = null
    var offset: Int? = null
}
