package com.gmail.ivanov.erik.coverageArea.domain.param;

public class Filter {

	public String getField() {return this.field;}

	public Object getValue() {return this.value;}

	public String getOp() {return this.op;}

	public Enum<SqlSuffix> getSuffix() {return this.suffix;}

	public void setField(String field) {this.field = field; }

	public void setValue(Object value) {this.value = value; }

	public void setOp(String op) {this.op = op; }

	public void setSuffix(Enum<SqlSuffix> suffix) {this.suffix = suffix; }

	public enum SqlSuffix {OR, AND, CLOSE}

	private String field;
	private Object value;
	private String op;
	private Enum<SqlSuffix> suffix = SqlSuffix.AND ;

	public Filter(Filter filter) {
		field = filter.field;
		value = filter.value;
		op = filter.op;
		suffix = filter.suffix;
	}

	public Filter(String fieldName, Object value, String operation, SqlSuffix suffix) {
		this.field = fieldName;
		this.value = value;
		this.op = operation;
		this.suffix = suffix;
	}

	public Filter() {
	}

	public String getSqlSuffix() {
		return suffix == SqlSuffix.CLOSE ? "" : suffix.toString();
	}
}
