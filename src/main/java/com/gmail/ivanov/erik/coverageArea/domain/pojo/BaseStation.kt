package com.gmail.ivanov.erik.coverageArea.domain.pojo

import java.util.*

data class BaseStation(var name: String, var detectionRadiusInMeters: Float, override var id: UUID, override var x: Float, override var y: Float): Station() {
    constructor(): this("", 0f, UUID(0, 0) , 0f, 0f)

    fun calculateDistance(mobileStation: MobileStation): Float {
        return Math.sqrt(Math.pow((mobileStation.x - x).toDouble(), 2.0) + Math.pow((mobileStation.y - y).toDouble(), 2.0)).toFloat()
    }

}
