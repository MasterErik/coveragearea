package com.gmail.ivanov.erik.coverageArea.service.impl;

import com.gmail.ivanov.erik.coverageArea.domain.Constant;
import com.gmail.ivanov.erik.coverageArea.domain.Errors;
import com.gmail.ivanov.erik.coverageArea.domain.param.MapParam;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.BaseStation;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.CalculateReport;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.MobileStation;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.ReportMessage;
import com.gmail.ivanov.erik.coverageArea.mapper.MobileStationMapper;
import com.gmail.ivanov.erik.coverageArea.mapper.ReportMapper;
import com.gmail.ivanov.erik.coverageArea.service.MobileService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class MobileServiceImpl implements MobileService {

    private final ReportMapper report;
    private final MobileStationMapper mobileStationMapper;

    public MobileServiceImpl(ReportMapper report, MobileStationMapper mobileStationMapper) {
        this.report = report;
        this.mobileStationMapper = mobileStationMapper;
    }
    @Transactional
    public void insert(ReportMessage reportMassage) {
        report.insert(reportMassage);
    }
    private MobileStation getStation(MapParam param) {
        return mobileStationMapper.getOne(param);
    }
    private List<CalculateReport> getReportMessageList(MapParam param) {
        return report.getMessage(param);
    }

    private boolean checkMultiDetermined(List<CalculateReport> list) {
        long multiDetermined = list.stream().filter(e -> e.getReports().size() > 1).count();
        return multiDetermined > 0;
    }

    private boolean checkDistance(BaseStation baseStation, MobileStation mobileStation, Float defDistance) {
        Float calcDistance = baseStation.calculateDistance(mobileStation);
        boolean result = true;

        if (calcDistance > baseStation.getDetectionRadiusInMeters()) {
            mobileStation.setError_code(Errors.DETECTION_RANGE.getId());
            mobileStation.setError_description(Errors.DETECTION_RANGE.getMessage());
            mobileStation.setError_radius(Math.abs(baseStation.getDetectionRadiusInMeters() - defDistance));
            result = false;
        } else if (Math.abs(calcDistance/defDistance - 1) > Constant.EPSILON ) {
            mobileStation.setError_code(Errors.CALC_DISTANCE.getId());
            mobileStation.setError_description(Errors.CALC_DISTANCE.getMessage());
            mobileStation.setError_radius(Math.abs(calcDistance - defDistance));
            result = false;
        }
        return result;
    }

    private void checkDistance(MobileStation mobileStation, List<CalculateReport> list) {
        list.forEach(e -> {
            BaseStation baseStation = new BaseStation("", e.getDetectionRadiusInMeters(), e.getBase_station_id(), e.getX(), e.getY());
            e.getReports().forEach( item -> checkDistance(baseStation, mobileStation, item.getDistance()));
        });
    }
    @Transactional(readOnly = true)
    public MobileStation getCoordinatesMobileStation(UUID mobileStationId) {
        MapParam param = new MapParam();
        param.setFilter("station", mobileStationId);
        MobileStation mobileStation = getStation(param);
        if (mobileStation == null) {
            mobileStation = new MobileStation();
        }
        List<CalculateReport> list = getReportMessageList(param);

        if (list == null || list.isEmpty() || list.get(0).getReports().isEmpty()) {

            mobileStation.setError_code(Errors.NOT_FOUND.getId());
            mobileStation.setError_description(Errors.NOT_FOUND.getMessage());
        } else if (checkMultiDetermined(list)) {
            mobileStation.setError_code(Errors.REPEATEDLY.getId());
            mobileStation.setError_description(Errors.REPEATEDLY.getMessage());
        } else {
            checkDistance(mobileStation, list);
        }
        return mobileStation;
    }
}
