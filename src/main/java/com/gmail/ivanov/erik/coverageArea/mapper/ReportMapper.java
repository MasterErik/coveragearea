package com.gmail.ivanov.erik.coverageArea.mapper;

import com.gmail.ivanov.erik.coverageArea.domain.param.MapParam;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.CalculateReport;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.ReportMessage;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ReportMapper {
    Integer count(MapParam param);
    void insert(ReportMessage message);
    List<CalculateReport> getMessage(MapParam param);
}
