package com.gmail.ivanov.erik.coverageArea.domain;

public enum Errors {
    NOT_FOUND(10, "Not found mobile station"),
    REPEATEDLY(11, "Mobile station is determined repeatedly"),
    CALC_DISTANCE(100, "Calculated by the coordinates of the distance does not match the specified"),
    DETECTION_RANGE(101, "Distance to the mobile station is greater than the detection range");

    private final int id;
    private final String message;

    Errors(int id, String message) {
        this.id = id;
        this.message = message;
    }

    public int getId() { return id; }
    public String getMessage() { return message; }

}
