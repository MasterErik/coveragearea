package com.gmail.ivanov.erik.coverageArea.domain.pojo

import java.util.*

data class MobileStation(override var id: UUID, override var x: Float, override var y: Float) : Station() {
    constructor(): this(UUID(0,0), 0f, 0f)
    val mobileId: UUID
        get() = this.id

    var error_radius: Float? = null
    var error_code: Int? = null
    var error_description: String? = null
}
