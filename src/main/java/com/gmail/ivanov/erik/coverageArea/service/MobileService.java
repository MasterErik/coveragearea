package com.gmail.ivanov.erik.coverageArea.service;

import com.gmail.ivanov.erik.coverageArea.domain.pojo.MobileStation;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.ReportMessage;

import java.util.UUID;

public interface MobileService {
    void insert(ReportMessage reportMassage);
    MobileStation getCoordinatesMobileStation(UUID mobileStationId);

}
