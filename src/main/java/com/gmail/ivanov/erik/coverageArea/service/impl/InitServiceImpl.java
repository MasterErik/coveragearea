package com.gmail.ivanov.erik.coverageArea.service.impl;

import com.gmail.ivanov.erik.coverageArea.helper.GenerateData;
import com.gmail.ivanov.erik.coverageArea.mapper.BaseStationMapper;
import com.gmail.ivanov.erik.coverageArea.mapper.MobileStationMapper;
import com.gmail.ivanov.erik.coverageArea.mapper.ReportMapper;
import com.gmail.ivanov.erik.coverageArea.service.InitService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class InitServiceImpl implements InitService {

    private final BaseStationMapper baseStationMapper;
    private final MobileStationMapper mobileStationMapper;
    private final ReportMapper ReportMapper;

    public InitServiceImpl(ReportMapper report, BaseStationMapper baseStationMapper, MobileStationMapper mobileStationMapper) {
        this.ReportMapper = report;
        this.baseStationMapper = baseStationMapper;
        this.mobileStationMapper = mobileStationMapper;
    }

    public void initialize() {
        GenerateData generateData = new GenerateData();
        generateData.generate();
        generateData.getBaseStationList().forEach(baseStationMapper::insert);
        generateData.getMobileStationList().forEach(mobileStationMapper::insert);
        generateData.getReportList().forEach(ReportMapper::insert);
    }


}
