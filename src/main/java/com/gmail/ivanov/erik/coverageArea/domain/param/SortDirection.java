package com.gmail.ivanov.erik.coverageArea.domain.param;

public enum SortDirection {
    ASC("Ascending"),
    DESC("Descending");

    private final String text;

    SortDirection(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
