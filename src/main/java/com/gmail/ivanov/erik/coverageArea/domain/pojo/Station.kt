package com.gmail.ivanov.erik.coverageArea.domain.pojo

import java.util.*

abstract class Station {
    abstract var id: UUID
    abstract var x: Float
    abstract var y: Float
}
