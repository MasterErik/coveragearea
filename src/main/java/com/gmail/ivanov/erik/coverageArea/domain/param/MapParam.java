package com.gmail.ivanov.erik.coverageArea.domain.param;


import com.gmail.ivanov.erik.coverageArea.domain.Constant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapParam extends Paging {

    private List<Long> masterListId;

    private Map<String, Object> filters = new HashMap<>();
    private Map<String, List<Filter>> criteria = new HashMap<>();
    private Map<String, List<Object>> criteriaIn = new HashMap<>();
    private String keywordSearch;
    private Map<String, String> properties = new HashMap<>();


    public void setTable(String name) {
        setFilter(Constant.TABLE_NAME, name);
    }

    public void setFilter(String name, Object value) {
        filters.put(name, value);
    }

    public void setFilter(Object value) {
        filters.put(Constant.DEFAULT_CRITERIA, value);
    }

    public Object getFilter(String name) {
        return filters.get(name);
    }

    public void setCriteria(String name, List<Filter> value) {
        criteria.put(name, value);
    }

    public void clearFilters() {
        filters.clear();
    }

    public void clearCriteria() {
        criteria.clear();
    }

    public void addCriteria(Filter value) {
        String name = Constant.DEFAULT_CRITERIA;
        addCriteria(name, value);
    }

    public void addCriteria(String name, Filter value) {
        if (criteria.containsKey(name)) {
            criteria.get(name).add(value);

        } else {
            List<Filter> listFilter = new ArrayList<>();
            listFilter.add(value);
            criteria.put(name, listFilter);
        }
    }

    public void addCriteriaIn(String name, List<Object> value) {
        criteriaIn.put(name, value);
    }

    public void prepareValue() {
        criteria.forEach((k, v) -> {
            if (v != null && !v.isEmpty()) {
                Filter filter = v.get(v.size() - 1);
                filter.setSuffix(Filter.SqlSuffix.CLOSE);
            }
        });
    }

    public Map<String, List<Filter>> getCriteria() {
        return criteria;
    }

}
