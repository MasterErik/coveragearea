package com.gmail.ivanov.erik.coverageArea.config;

import com.gmail.ivanov.erik.coverageArea.typeHandler.UUIDTypeHandler;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.UUID;

import static org.apache.ibatis.session.AutoMappingUnknownColumnBehavior.FAILING;
import static org.apache.ibatis.session.ExecutorType.REUSE;

@Configuration
@EnableTransactionManagement
public class TypeHandlerConfiguration {


    @Qualifier("dataSource")
    @Autowired
    private DataSource dataSource;

    public static void initConfig(org.apache.ibatis.session.Configuration configuration) {
        configuration.setDefaultExecutorType(REUSE);
        configuration.setAutoMappingUnknownColumnBehavior(FAILING);
        configuration.setMultipleResultSetsEnabled(true);
        configuration.setUseGeneratedKeys(true);
        configuration.setCallSettersOnNulls(true);
        configuration.setMapUnderscoreToCamelCase(true);
        configuration.setCacheEnabled(true);
        configuration.setLazyLoadingEnabled(false);
        configuration.getTypeHandlerRegistry().register(UUID.class, UUIDTypeHandler.class);
    }
    @Bean
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        //DataSource dataSource = DataSourceBuilder.create().build();
        sqlSessionFactoryBean.setDataSource(dataSource);
        initConfig(sqlSessionFactoryBean.getObject().getConfiguration());
        return sqlSessionFactoryBean.getObject();
    }

}
