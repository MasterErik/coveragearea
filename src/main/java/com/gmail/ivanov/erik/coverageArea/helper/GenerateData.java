package com.gmail.ivanov.erik.coverageArea.helper;

import com.gmail.ivanov.erik.coverageArea.domain.pojo.BaseStation;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.Message;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.MobileStation;
import com.gmail.ivanov.erik.coverageArea.domain.pojo.ReportMessage;
import org.fluttercode.datafactory.impl.DataFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GenerateData {

    protected static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private final DataFactory df = new DataFactory();
    private final List<BaseStation> baseStationList = new ArrayList<>();
    private final List<MobileStation> mobileStationList = new ArrayList<>();
    private final List<ReportMessage> reportList = new ArrayList<>();

    private int limitBase = 10;
    private int limitMobile = 20;
    private int limitReport = 10;
    private int limitMessage = 5;

    protected void genBaseStation(int limit) {
        baseStationList.clear();
        for (int i = 0; i < limit; i++) {
            BaseStation baseStation = new BaseStation(df.getCity(), df.getNumberBetween(10, 100), UUID.randomUUID(), df.getNumberBetween(0, 1000), df.getNumberBetween(0, 1000));
            baseStationList.add(baseStation);
        }
    }

    protected void genMobileStation(int limit) {
        mobileStationList.clear();
        for (int i = 0; i < limit; i++) {
            MobileStation mobileStation = new MobileStation(UUID.randomUUID(), df.getNumberBetween(0, 1000), df.getNumberBetween(0, 1000));
            mobileStationList.add(mobileStation);
        }
    }
    protected UUID getRandomBaseStation() {
        int size = baseStationList.size();
        int index = Random.rnd(size);
        return baseStationList.get(index).getId();
    }
    protected UUID getRandomMobileStation() {
        int size = mobileStationList.size();
        int index = Random.rnd(size);
        return mobileStationList.get(index).getId();
    }

    protected void genReport(int limit, int limitMessage) {
        reportList.clear();
        int count = 0;
        for (int i = 0; i < limit; i++) {
            ReportMessage reportMessage = new ReportMessage(getRandomBaseStation());
            reportList.add(reportMessage);
            UUID mobileStationId = getRandomMobileStation();
            for (int j = 0; j < limitMessage; j++) {
                count++;
                final Message message = new Message(mobileStationId, Random.rnd(300), new Timestamp(count*10000));
                reportMessage.getReports().add(message);
            }
        }
    }

    public void generate(int limitBase, int limitMobile, int limitReport, int limitMessage) {
        this.limitBase = limitBase;
        this.limitMobile = limitMobile;
        this.limitReport = limitReport;
        this.limitMessage = limitMessage;
        generate();
    }

    public void generate() {
        genBaseStation(limitBase);
        genMobileStation(limitMobile);
        genReport(limitReport, limitMessage);
    }

    public List<BaseStation> getBaseStationList() {
        return baseStationList;
    }
    public List<MobileStation> getMobileStationList() {
        return mobileStationList;
    }
    public List<ReportMessage> getReportList() {
        return reportList;
    }
}
